COMMONFLAGS = -DUNIX -DINLINE_G=inline -D_GNU_SOURCE
CMD_FLAGS = -DGLBSP_TEXT $(COMMONFLAGS)
LIB_FLAGS = -DGLBSP_PLUGIN $(COMMONFLAGS)
AR = ar rc
RANLIB = ranlib

version ?= 3.0.0
major ?= 3
CFLAGS ?= -O2 -Wall

LIB_OBJS=\
	analyze.o  \
	blockmap.o \
	glbsp.o    \
	level.o    \
	node.o     \
	reject.o   \
	seg.o      \
	system.o   \
	util.o     \
	wad.o

LIB_LIBS = -lm -lz

SHARED_OBJS=$(addprefix shared/,$(LIB_OBJS))
STATIC_OBJS=$(addprefix static/,$(LIB_OBJS))

CMD_OBJS=display.o main.o

all: libglbsp.so.$(version) libglbsp.a glbsp

clean:
	rm -rf libglbsp.* shared static glbsp cmdline/*.o

shared static:
	mkdir -p $@
	cd $@ && ln -s ../src/*.[ch] .

libglbsp.so.$(version): shared/libglbsp.so
	ln -s $< $@
shared/libglbsp.so: shared
	$(MAKE) -C shared -f ../debian/makefile libglbsp.so \
		CFLAGS="$(CFLAGS) $(LIB_FLAGS) -shared -fPIC"
libglbsp.so: $(LIB_OBJS)
	$(CC) $^ -o $@ $(LIB_LIBS) -shared -fPIC -Wl,-soname,$@.$(major) \
		-Wl,--version-script=../debian/version.ld

libglbsp.a: static/libglbsp.o
	ln $< $@
static/libglbsp.o: static
	$(MAKE) -C static -f ../debian/makefile libglbsp.o \
		CFLAGS="$(CFLAGS) $(LIB_FLAGS) -static"
libglbsp.o: $(LIB_OBJS)
	$(AR) $@ $^
	$(RANLIB) $@

glbsp: cmdline/_glbsp.o libglbsp.so.$(version)
	$(CC) $^ -o $@
cmdline/_glbsp.o:
	$(MAKE) -C cmdline -f ../debian/makefile _glbsp.o \
		CFLAGS="-I../src $(CFLAGS) $(CMD_FLAGS)"
_glbsp.o: $(CMD_OBJS)
	$(AR) $@ $^
	$(RANLIB) $@

install: all
	install -d $(DESTDIR)/usr/bin
	install -d $(DESTDIR)/usr/include/glbsp
	install -d $(DESTDIR)/usr/lib
	install libglbsp.so.$(version) libglbsp.a $(DESTDIR)/usr/lib
	ln -s libglbsp.so.$(version) $(DESTDIR)/usr/lib/libglbsp.so.$(major)
	ln -s libglbsp.so.$(major) $(DESTDIR)/usr/lib/libglbsp.so
	install glbsp $(DESTDIR)/usr/bin
	install -m644 src/glbsp.h $(DESTDIR)/usr/include/glbsp
	ln -s glbsp/glbsp.h $(DESTDIR)/usr/include/glbsp.h

.PHONY: all clean

.NOTPARALLEL:
