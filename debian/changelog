glbsp (2.24-5) unstable; urgency=medium

  * Remove myself from Uploaders.

 -- Jonathan Dowland <jmtd@debian.org>  Mon, 11 May 2020 10:26:59 +0100

glbsp (2.24-4) unstable; urgency=medium

  * Provide doom-node-builder and alternative for /usr/bin/bsp, so
    glbsp can be interchanged with (e.g.) zdbsp by doom map editors
    (e.g. wadc)
  * Add Vcs-* headers

 -- Jonathan Dowland <jmtd@debian.org>  Tue, 20 Nov 2018 16:41:41 +0000

glbsp (2.24-3) unstable; urgency=medium

  * Adopt package. Closes: #881508.

 -- Jonathan Dowland <jmtd@debian.org>  Wed, 31 Jan 2018 22:02:20 +0000

glbsp (2.24-2) unstable; urgency=low

  * Change of maintainer address.
  * Bump standards version to 3.9.3 & dh compat to 5.
    - Add versioned dep on dpkg-dev.
    - libglbsp3-dev: depend on ${misc:Depends} & tighten dep on libglbsp3.
  * Use dpkg-buildflags.

 -- Darren Salt <devspam@moreofthesa.me.uk>  Tue, 20 Nov 2012 20:50:40 +0000

glbsp (2.24-1) unstable; urgency=low

  * New upstream release.
  * Bumped the lib soname and the library package name due to one silly
    little binary incompatibility caused by changes in an exported struct.
    (Safe; nothing else currently in the archive has ever used libglbsp2.)
  * Removed my patches since they're all applied upstream.
  * Updated the list of documentation files.
  * Build-time changes:
    - Switched from dh_movefiles to dh_install.
    - Updated my makefile to cope with upstream changes.
    - Corrected for debian-rules-ignores-make-clean-error.
    - Corrected for substvar-source-version-is-deprecated.
    - Link libglbsp, rather than glbsp, with libm and libz.
  * Fixed shlibdeps. (Closes: #460387)
  * Bumped standards version to 3.7.3 (no other changes).

 -- Darren Salt <linux@youmustbejoking.demon.co.uk>  Wed, 30 Jan 2008 13:33:49 +0000

glbsp (2.20-4) unstable; urgency=low

  * Uploaded to Debian (closes: #368221)
  * Bumped standards version to 3.7.2 (no other changes).
  * Converted to dpatch.
  * Added a patch to fix assumption about map data lump order, allowing
    correct processing of an example WAD supplied by Jon Dowland.
  * Added a patch to get rid of sprintf(), vsprintf(), strcpy() and strcat(),
    preferring the likes of asprintf(), where there is the possibility of
    buffer overflow.

 -- Darren Salt <linux@youmustbejoking.demon.co.uk>  Sun, 13 Aug 2006 12:38:26 +0100

glbsp (2.20-3) unstable; urgency=low

  * Tweaked the package descriptions slightly.

 -- Darren Salt <linux@youmustbejoking.demon.co.uk>  Sat, 20 May 2006 17:46:06 +0100

glbsp (2.20-2) unstable; urgency=low

  * Moved glbsp.h into /usr/include/glbsp/; symlink in the old location.

 -- Darren Salt <linux@youmustbejoking.demon.co.uk>  Thu, 26 Jan 2006 00:10:58 +0000

glbsp (2.20-1) unstable; urgency=low

  * New upstream release.
  * Added a version script to remove namespace pollution.

 -- Darren Salt <linux@youmustbejoking.demon.co.uk>  Sun, 18 Sep 2005 18:59:53 +0100

glbsp (2.10c-1) unstable; urgency=low

  * New upstream release.
  * Removed debian/glbsp.1.

 -- Darren Salt <linux@youmustbejoking.demon.co.uk>  Fri, 01 Oct 2004 21:59:41 +0100

glbsp (2.05-1) unstable; urgency=low

  * Initial Release.

 -- Darren Salt <linux@youmustbejoking.demon.co.uk>  Thu, 05 Feb 2004 20:00:15 +0000

